import { CommandConstants } from ".";

export type CommandFormat = { commandName: string; args: string[] };
/**
 * This parses a command in a format like: {prefix}{command}{args}
 * Taking the first argSeparator as the command.
 * @param input
 */
export const parseCommand = (input: string): CommandFormat => {
    const command = input.slice(CommandConstants.prefix.length);
    const body = command.split(CommandConstants.prefix);
    return {
        commandName: body[0],
        args: body.slice(1),
    };
};
