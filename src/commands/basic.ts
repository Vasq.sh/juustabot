import Discord from "discord.js";
import { CommandFormat } from "./parse";

export enum EBasicCommandNames {
    PING = "ping",
    SUM = "sum",
}

type deps = { command: CommandFormat; message: Discord.Message };

const ping = ({ message }: deps) => {
    const timeTaken = Date.now() - message.createdTimestamp;
    message.reply(`Pong! This message had a latency of ${timeTaken}ms.`);
};

const sum = ({ command, message }: deps) => {
    const numArgs = command.args.map((x) => parseFloat(x));
    const sum = numArgs.reduce((counter, x) => (counter += x));
    message.reply(`The sum of all the arguments you provided is ${sum}!`);
};

/**
 * Runs the basic commands module with a message
 * @param commandName command used to identify the function to run
 * @param deps {CommandFormat, Discord.message} Needed to make the module work
 */
const start = (commandName: string, deps: deps): void => {
    switch (commandName) {
        case EBasicCommandNames.PING:
            return ping(deps);
        case EBasicCommandNames.SUM:
            return sum(deps);
    }
};

export const BasicCommand = { ping, sum, start };
