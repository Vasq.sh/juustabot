import Discord from "discord.js";
import { CommandFormat } from "../parse";
import { createAxios } from "../../http/axios";

export enum EJuustagramCommandNames {
    FEED = "feed",
    LATEST = "latest",
    RANDOM = "random",
}

type deps = { command: CommandFormat; message: Discord.Message };
type JuustagramResponse = { entries: JuustagramPost[] };

interface JuustagramPost extends JuustagramReply {
    id: number;
    post: string;
    picture: string;
}

interface JuustagramReply {
    name: string;
    icon: string;
    post: string;
    replies: JuustagramReply[];
}

const feed = async ({}: deps) => {};
const random = async ({ message }: deps) => {
    const response: JuustagramResponse = await createAxios().get("https://nimiii.now.sh/api/social/");
    const entries = response.entries.filter((entry) => entry.post);
    const latest = entries[Math.floor(Math.random() * (entries.length - 1)) + 1];
    const messageEmbed = new Discord.MessageEmbed().setAuthor(latest.name).setImage(latest.picture).setFooter(latest.post);
    message.channel.send(messageEmbed);
};

const latest = async ({ message }: deps) => {
    const response: JuustagramResponse = await createAxios().get("https://nimiii.now.sh/api/social/");
    const entries = response.entries.filter((entry) => entry.post);
    const latest = entries[entries.length - 1];
    const messageEmbed = new Discord.MessageEmbed().setAuthor(latest.name).setImage(latest.picture).setFooter(latest.post);
    message.channel.send(messageEmbed);
};

/**
 * Runs the basic commands module with a message
 * @param commandName command used to identify the function to run
 * @param deps {CommandFormat, Discord.message} Needed to make the module work
 */
const start = async (command: CommandFormat, deps: deps): Promise<void> => {
    switch (command.commandName) {
        case EJuustagramCommandNames.FEED:
            return await feed(deps);
        case EJuustagramCommandNames.LATEST:
            return await latest(deps);
        case EJuustagramCommandNames.RANDOM:
            return await random(deps);
    }
};

export const Juustagram = { random, feed, latest, start };
