import Axios, { AxiosRequestConfig } from "axios";

export const createAxios = (config?: AxiosRequestConfig) => {
    const axios = Axios.create(config);

    const get = async <T>(url: string, config?: AxiosRequestConfig): Promise<T> => {
        try {
            return (await axios.get<T>(url, config)).data;
        } catch (error) {
            console.error(error);
            throw error;
        }
    };
    return { get };
};
