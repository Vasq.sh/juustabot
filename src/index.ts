import Discord from "discord.js";
import { CommandConstants } from "./commands";
import { BasicCommand } from "./commands/basic";
import { Juustagram } from "./commands/nimi/juustagram";
import { CommandFormat, parseCommand } from "./commands/parse";
import config from "../config.json";

const client = new Discord.Client();

client.on("ready", () => {
    console.log("Hi!, I am ready!");
});

client.on("message", (message: Discord.Message) => {
    if (message.author.bot) return; // Check if its a bot who writes
    if (!message.content.startsWith(CommandConstants.prefix)) return; // Check if message uses prefix

    const command: CommandFormat = parseCommand(message.content);

    BasicCommand.start(command.commandName, { command, message });
    Juustagram.start(command, { command, message });
});

client.login(config.BOT_TOKEN);
